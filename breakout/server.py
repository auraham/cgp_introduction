# server.py
from __future__ import print_function
import os, sys

# add cgp to path
path_script = os.path.dirname(os.path.abspath(__file__))
path_games = os.path.dirname(path_script)
path_cgp = os.path.join(path_games, "flappy_bird_cgp")
sys.path.insert(0, path_cgp)

from flask import Flask
from flask import request, make_response
from flask import jsonify
from flask import Response
from numpy.random import RandomState
from flask_cors import CORS
import cgp_debug as cgp
from unfold import unfold_node
app = Flask(__name__)
app.debug = True
CORS(app)

cache = {"counter": 0}
rand = RandomState(100)


# create the initial population
pop_size = 10       # number of players
n_inputs = 4        # number of inputs from game

cache = {
    "pop" : None,
}

@app.route("/")
def index():
    """
    This method displays the current population
    """
    
    # --- this block simulates a generation ---
    
    pop = cache["pop"]
    
    if pop is not None:
        return str(pop)
        
    return "pop is None"

def str_vector(vec):
    
    return "["+" ".join(["%.4f" % val for val in vec])+"]"

def str_vector_line(vec):
    
    return " ".join(["%.4f" % val for val in vec]) + "\n"
    
@app.route("/evolution", methods=["GET", "POST"])
def evolution():
    """
    This method 
    - receives the scores (fitness values) of each individual in the population
    - select the best mu individuals
    - creates a new population from the best individuals
    - returns a signal to the client when evolution is finished
    """

    scores = None
    
    if request.method == "GET":
        
        # http://localhost:5000/evolution?scores=1,2,3
        # get params as a single string
        scores = request.args.get("scores", None)
        
        # create list of params
        scores = scores.split(",")
        
        # convert to list of ints
        scores = [float(v) for v in scores]
        
        print("get scores          ", str_vector(scores))
        print("get scores type     ", type(scores))
        print("get scores type item", type(scores[0]))

    if request.method == "POST":
        
        # input {"scores":"[1,2,3,4]"}
        scores = request.json.get("scores", None)
    
        # remove [ ]
        scores = scores.replace("[", "").replace("]", "")
        
        # create list of params
        scores = scores.split(",")
        
        # convert to list of ints
        scores = [float(v) for v in scores]
        
        print("post scores          ", str_vector(scores))
        print("post scores type     ", type(scores))
        print("post scores type item", type(scores[0]))
    
    
    # simulate work
    #import time; time.sleep(5);
    
    # ---
    if cache["pop"] is None:
        cache["pop"] = cgp.create_population(pop_size, n_inputs)

    
    # current pop in cache
    cur_pop = cache["pop"]

    # assign fitness/scores to individuals
    for i in range(len(cur_pop)):
        cur_pop[i].fitness = scores[i]
    
    pb = 0.15              # probability of mutation
    n_parents = 2           # number of parents
    n_offspring = 8         # number of offspring
    new_pop = cgp.evolve(cur_pop, pb, n_parents, n_offspring)
    
    # call evaluate to determine active nodes
    x = 10                                  # use any number here
    y = 20
    w = 10
    z = 20
    for ind in new_pop:
        ind.eval(x, y, w, z)
    
    # update cache
    cache["pop"] = new_pop
    
    fs = ["+", "-", "*", "/", "-"]          # function set
    leaves = {
            -1: "a",    # ball.x
            -2: "b",    # ball.y
            -3: "c",    # paddle.x
            -4: "d"}    # paddle.y
    
    # return the model of each player
    data = { "model_%s"%i : unfold_node(new_pop[i].nodes, fs, leaves) for i in range(pop_size)}
    return jsonify(data)
    
    
@app.route("/save_points", methods=["GET", "POST"])
def save_points():
    """
    This method 
    - receives the scores (fitness values) of each individual in the population
    - select the best mu individuals
    - creates a new population from the best individuals
    - returns a signal to the client when evolution is finished
    """

    values = None
    
    if request.method == "GET":
        
        # http://localhost:5000/save_point?params=1,2,3,4,5
        # get params as a single string
        # p1 = [params[0], params[1]]
        # p2 = [params[2], params[3]]
        # x  = params[4] 
        params = request.args.get("params", None)
        
        # create list of params
        values = params.split(",")
        
        # convert to list of ints
        values = [float(v) for v in values]
        
        print("get values          ", str_vector(values))
        print("get values type     ", type(values))
        print("get values type item", type(values[0]))

    if request.method == "POST":
        
        # input {"scores":"[1,2,3,4]"}
        params = request.json.get("params", None)
        
        # remove [ ]
        values = params.replace("[", "").replace("]", "")
        
        # create list of params
        values = values.split(",")
        
        # convert to list of ints
        values = [float(v) for v in values]
        
        print("post params          ", str_vector(values))
        print("post params type     ", type(values))
        print("post params type item", type(values[0]))
    
    # save point
    with open("points.txt", "a") as log:
        log.write(str_vector_line(values))
    
    return str_vector(values)
    
    
@app.route("/action", methods=["GET", "POST"])
def action():
    """
    This method
    - receives the state of the environment
    - returns an action 
    """
    
    if request.method == "POST":
        
        # input {"scores":"[1,2,3,4]"}
        x = request.json.get("x", None)
        y = request.json.get("y", None)
    
        print("x", x)
        print("x type", type(x))
        
        print("y", y)
        print("y type", type(y))
    
        # remove [ ]
        #scores = scores.replace("[", "").replace("]", "")
        
        # create list of params
        #scores = scores.split(",")
        
        # convert to list of ints
        #scores = [int(v) for v in scores]
        
        #print("post scores          ", scores)
        #print("post scores type     ", type(scores))
        #print("post scores type item", type(scores[0]))
    
    move = "right"
    
    if rand.rand() < 0.5:
        move = "left"
    
    data = {"response": move}
    return jsonify(data)
    
    
