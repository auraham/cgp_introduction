# Breakout

This directory contains an implementation of Breakout. To more details about this implementation, [follow this tutorial from Mozilla](https://developer.mozilla.org/en-US/docs/Games/Tutorials/2D_Breakout_game_pure_JavaScript).



## Scripts

| Name             | Description                                                  |
| ---------------- | ------------------------------------------------------------ |
| `index.html`     | Main file. Run `server.py` and open this file in your browser to start the game. |
| `index_fit.html` | Similar to `index.html`. This file plays breakout using an predefined agent found via CGP. Run `server_fit.py` and open this file in your browser to start the game. |
| `style.css`      | CSS styles                                                   |
| `server.py`      | Proxy between breakout (javascript) and CGP (python). This script receive the scores of the players and return a new population of agents for playing breakout. |
| `server_fit.py`  | Similar to `server.py`. This file returns an predefined agent found via CGP. |



## Start the server

Requirements:

```
pip install flask
pip install flask_cors
```

Run server:

```
cd breakout
export FLASK_APP=server_fit.py
export FLASK_ENV=development
flask run
```

Now, open `http://127.0.0.1:5000` in your browser. Then, open `index_fit.html` in the browser. You will see how the player breaks the bricks automatically:

![](../img/breakout.gif)


The predefined agent found using CGP is encoded as follows:

```python
# server_fit.py
def evolution():
    # ...
    
    # trained model
    model = "Math.abs(a-f+b-Math.sin(a)) + Math.pow(((Math.sin(a) - d-2*b) / (Math.sin(a)-b)), 2)"
```

The input variables are defined as follows:

```python
leaves = {
            -1: "a",    # point a.x
            -2: "b",    # point a.y
            -3: "c",    # point b.x
            -4: "d",    # point b.y
            -5: "e",    # canvas.width
            -6: "f"     # canvas.height
}
```

The expression encoded in `model` is evaluated in javascript using `eval()`:

```javascript
// index_fit.html
function moveController(index) {
	// This function evaluates the model/player
            
	// inputs
    a = x_prev;
    b = y_prev;
    c = x_cur;
    d = y_cur;
    e = canvas.width;
    f = canvas.height;
           
    // evaluate model from inputs
    result = eval(model);
    
    // move paddle
    paddleX = result;
    paddleX = result;
}
```








