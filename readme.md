# Introduction to Cartesian Genetic Programming

This repository contains my notes about Cartesian Genetic Programming (CGP). Here, we train an agent (player) for playing two games using CGP:

- Flappy bird
- Breakout



## Run flappy bird

In order to run train an agent using CGP for playing Flappy bird, run the following commands:

```
cd flappy_bird_cgp
python3 main.py
```

Here, you can see how a few birds (players) can fly avoiding the pipes:

![](img/flappy.gif)



## Run Breakout

Run the following commands to play Breakout using a player trained via CGP:

```
cd breakout
export FLASK_APP=server_fit.py
export FLASK_ENV=development
flask run
```

Now, open `http://127.0.0.1:5000` in your browser. Then, open `index_fit.html`  and you will see how the player breaks the bricks automatically:



![](img/breakout.gif)





## Expo

Compile `expo.tex` as follows:

```
cd expo
pdflatex -shell-escape expo.tex
bibtex expo.aux
pdflatex -shell-escape expo.tex
pdflatex -shell-escape expo.tex
```




## Contact

Auraham Camacho `auraham.cg@gmail.com`



## Credits

The implementation of CGP and Flappy bird was developed by [Shuhua Gao](https://github.com/ShuhuaGao). The code is available on [github](https://github.com/ShuhuaGao/gpFlappyBird).

The implementation of Breakout is part of a tutorial of Mozilla. The code is available [here](https://developer.mozilla.org/en-US/docs/Games/Tutorials/2D_Breakout_game_pure_JavaScript).