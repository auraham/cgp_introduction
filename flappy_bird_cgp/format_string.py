# format_string.py

def format_value(val):
    """
    Return a string representation of a single value
    """
    
    str_val = ""
    
    if isinstance(val, int):
        str_val = "{0:5d}".format(val)

    if isinstance(val, float):
        str_val = "{:7.2f}".format(val)

    if val is None:
        str_val = "   None"
            
    return str_val
    
    
def format_vector(vec):
    """
    Return a string representation of a vector
    """
    
    str_vec = [""] * len(vec)
    
    for i, val in enumerate(vec):
        
        if isinstance(val, int):
            str_vec[i] = "{0:5d}".format(val)
    
        if isinstance(val, float):
            str_vec[i] = "{:5.2f}".format(val)
    
        if val is None:
            str_vec[i] = " None"
            
    comb = ", ".join(str_vec)
    return "[%s]" % comb
    
if __name__ == "__main__":
    
    
    print(format_vector([-2, 2]))
    print(format_vector([-2, -2]))
    print(format_vector([1, 1]))
    print(format_vector([1, None]))
    print(format_vector([None, None]))
    print(format_vector([0.1, None]))
    print(format_vector([-0.1, 0.1]))
    print(format_vector([None, 0.1]))
    
    
    
