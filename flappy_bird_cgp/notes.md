# Notes

This document contains some notes about this project to know how it works.



## Main loop

This loops controls what we see every time the birds are flying:

```python
# main.py

def main():
    game = Game()
    while game.running and game.current_generation <= N_GEN:
        
        # clear all sprites
        # place self.n_birds randomly in the screen
        # place the pipes
        # draw the background
        game.reset()
        
        # 
        game.run()
```



## Update the screen

The birds and the pipes moves towards the left as long as there is at least one bird alive. The screen is updated according to this sequence of functions:

```python
# main.py

def main():
    game = Game()
    while game.running and game.current_generation <= N_GEN:
        
        # clear all sprites
        # place self.n_birds randomly in the screen
        # place the pipes
        # draw the background
        game.reset()
        
        # start the game and the evolutionary process
        game.run()
```

Then, go to `run()`:

```python
def run(self):
    self.playing = True
    while self.playing:
        self._handle_events()			# listener for key events
        self._update()					# update the locations of the pipes, so the birds seem to fly
        self._draw()                    # update the screen (move all objects to the left)
        self._clock.tick(self._fps)
    if not self.running:
        return
        
        # ...
        # evolution goes here ...
        # self.pop = cgp.evolve(self.pop, pb, MU, LAMBDA)
```

The important functions in this loop are `self._update()` and `self._draw()`. 

First, `self._update()` changes the locations of the pipes backwards (i.e., to the left), so the birds seem to fly. If there are no birds alive, then this function flips up a flag and the game is over.

```python
def _update(self):
    """
    Update the state (position, life, etc.) of all sprites and the game
    """
    
    self.all_sprites.update()
        
    # if all birds died, then game over
    if not self.birds:
        self.playing = False
        return
    
    # move the pipes backwards such that birds seem to fly
    leading_bird = max(self.birds, key=lambda b: b.rect.x)
    if leading_bird.rect.x < SCREEN_WIDTH / 3:
        for bird in self.birds:
            bird.moveby(dx=BIRD_X_SPEED)
    else:
        for pipe in self.pipes:
            pipe.moveby(dx=-BIRD_X_SPEED)
            if pipe.rect.x < -50:
           		pipe.kill()
    
    # count the score: one point per frame
    for bird in self.birds:
    	bird.score += 1  

    self._max_score += 1
    self._max_score_so_far = max(self._max_score_so_far, self._max_score)
    # spawn a new pipe if necessary
    while self._front_pipe.rect.x < SCREEN_WIDTH:
        self._spawn_pipe()
```

Notice that this function also updates the fitness of the birds. If the bird is still alive in the current frame, its fitness is incremented one unit:

```python
# count the score: one point per frame
for bird in self.birds:
	bird.score += 1  
```

You can check both the score and distance of the birds as follows:

```
ipdb> [bird.score for bird in self.birds]                                     
[6, 6, 6, 6, 6, 6, 6, 6, 6, 6]
```

```
ipdb> [bird.rect.x for bird in self.birds]                                    
[200, 56, 187, 91, 88, 40, 180, 24, 84, 78]
```

-----

**Note** The birds are stored in `self.birds`. Although it is iterable, it does not support indexing. Thus, you cannot access to the `i`-th bird using `self.birds[i]`:

```
ipdb> self.birds                                                              
<Group(10 sprites)>
ipdb> self.birds[0]                                                           
*** TypeError: 'Group' object does not support indexing
```

-----

On the other hand, `self._draw()` configures the text on the screen (current score, current generation, number of birds alive, etc.) . After defining all this, the screen is updated after calling `pg.display.update()`:

```python
def _draw(self):
    
    # new locations of the birds for the next time step
    self.all_sprites.draw(self._screen)

    # show score
    # ...

    # refresh screen
    pg.display.update()
```



## Evolutionary process

The evolutionary process takes place in `run()`:

```python
# game.py

def run(self):
    
    # this block handles the animation
    self.playing = True
    while self.playing:
        self._handle_events()
        self._update()
        self._draw() 
        self._clock.tick(self._fps)
    if not self.running:
        return
        
    # one generation finished and perform evolution again
    # if current score is very low, then we use a large mutation rate
    pb = MUT_PB
    if self._max_score < 500:
        pb = MUT_PB * 3
    elif self._max_score < 1000:
        pb = MUT_PB * 2
    elif self._max_score < 2000:
        pb = MUT_PB * 1.5
    elif self._max_score < 5000:
        pb = MUT_PB * 1.2
    
    # evolutionary process
    self.pop = cgp.evolve(self.pop, pb, MU, LAMBDA)
```

The first blocks handles the animation. That is, it moves the pipes backwards. Also, it updates the fitness of the birds. If all the birds failed, then the game is over.

```python
# game.py

def run(self):
    
    # this block handles the animation
    self.playing = True
    while self.playing:
        self._handle_events()
        self._update()
        self._draw() 
        self._clock.tick(self._fps)
    if not self.running:
        return
```

When the game is over, the evolutionary process starts. To do so, we first tune the mutation rate:

```python
# game.py

def run(self):
    
    # ... animation ...
    
    # one generation finished and perform evolution again
    # if current score is very low, then we use a large mutation rate
    pb = MUT_PB
    if self._max_score < 500:
        pb = MUT_PB * 3
    elif self._max_score < 1000:
        pb = MUT_PB * 2
    elif self._max_score < 2000:
        pb = MUT_PB * 1.5
    elif self._max_score < 5000:
        pb = MUT_PB * 1.2
```

Before going any further, let us introduce some concepts: generation/round and scores.

A **generation** is called a *round* in the code. One generation comprises three steps, described below in the `run()` function.

```python
def run(self):
    
    # 1. play the game, and wait it is over
    
    # 2. when the game is over, configure mutation rate
   
    # 3. create a new populations of birds to play the game in the next round
```

So, as you expect, in order to train/find/evolve a good player/bird, we need several rounds/generations. A new population of birds is given every generation. The number of rounds/generations is given in `settings.py`:

```python
# settings.py
# parameters of evolutionary strategy: MU+LAMBDA
MU = 2
LAMBDA = 8
N_GEN = 200  # max number of generations
```

and the function `run()` is called from `main.py`:

```python
# main.py

def main():
    game = Game()
    while game.running and game.current_generation <= N_GEN:
        game.reset()
        game.run()
```

Every iteration of this loop is a generation of the evolutionary process. On the other hand, there are two scores in the code, 

```python
# game.py

self._max_score_so_far = 0  # max score so far in all the rounds since the game started
self._max_score = 0         # max score of all the birds in this round (generation)
```

That is, `self._max_score` is the maximum score (among) among all the birds in the current generation. From our example above:

```python
ipdb> [bird.score for bird in self.birds]                                     
[6, 6, 6, 6, 6, 6, 6, 6, 6, 6]
```

`self._max_score` will be `6`. However, as the evolutionary algorithm creates better solutions, the fitness is expected to increase. Here, `self._max_score_so_far` saves the maximum score for all the birds since the game started. Suppose that the maximum score in generation `self.current_generation=10` is `100`:

```python
# scores in generation 100
ipdb> [bird.score for bird in self.birds]                                     
[40, 50, 60, 100, 80, 90, 40, 56, 35, 74]
```

Then, all the birds die and the game is restarted again. In the next generation, we can have this:

```python
# scores in generation 101
ipdb> [bird.score for bird in self.birds]                                     
[60, 20, 12, 23, 42, 54, 65, 76, 87, 12]
```

Notice that any of these birds have a score higher than `100`, as in the previous generation. The reason is as follows. Although the evolutionary algorithm is elitist (the fittest solution is always preserved), the score does not necessarily increases over time. This is because the environment changes every generation. Every time all the birds die, a new population is created (preserving the best `MU=2` solutions), the environment is created again (i.e., the locations and gaps of the pipes are created and they are not necessarily the same as in the previous generation), and the birds fly in the new environment. Since the configuration of the pipes changes every generation, we cannot expect that the fitness of the best individual/bird (`self._max_score`) increases over time. However, the best fitness gathered since the start of the game (`self._max_score_so_far`) must increase over time.

 Finally, we can evolve a population of birds:

```python
def run(self):
    
    # 1. play the game, and wait it is over
    
    # 2. when the game is over, configure mutation rate
    
    # 3. create a new populations of birds to play the game in the next round
    self.pop = cgp.evolve(self.pop, pb, MU, LAMBDA)
```

Let us take a look at the population:

```python
ipdb> self.pop                                                                
[<cgp.Individual object at 0x7f0210db3860>, 
<cgp.Individual object at 0x7f01e1899748>, 
<cgp.Individual object at 0x7f01e18406a0>, 
<cgp.Individual object at 0x7f01e18675f8>, 
<cgp.Individual object at 0x7f01e180f550>, 
<cgp.Individual object at 0x7f01e17b64a8>, 
<cgp.Individual object at 0x7f01e17dd400>, 
<cgp.Individual object at 0x7f01e1785358>, 
<cgp.Individual object at 0x7f01e17ac2b0>, 
<cgp.Individual object at 0x7f01e1754208>]
```

The population is a list of instances of `Individual`, defined in `cgp.py`:

```python
# cgp.py

class Individual:
    """
    An individual (chromosome, genotype, etc.) in evolution
    """
    function_set = None
    weight_range = [-1, 1]
    max_arity = 3
    n_inputs = 3
    n_outputs = 1
    n_cols = N_COLS
    level_back = LEVEL_BACK
```

Each individual contains a list of nodes (the number of nodes is determined by `N_COLS` in `settings.py`):

```python
ipdb> len(self.pop[0].nodes)                                          
500
```

Each node contains six attributes:

```python
# input function
ipdb> self.pop[0].nodes[0].i_func
1

# input parameters
ipdb> self.pop[0].nodes[0].i_inputs                                                     
[-1, -3]

# weights
ipdb> self.pop[0].nodes[0].weights                        
[0.6693339647391277, 0.027700185009203127]

# ?
ipdb> self.pop[0].nodes[0].i_output                                                
0

# output
ipdb> self.pop[0].nodes[0].output                                
201.0721387094124

# is this node active?
ipdb> self.pop[0].nodes[0].active                                                 
True
```



## Representation

In this section, we will talk about the representation of solutions. From [this tutorial](http://cs.ijs.si/ppsn2014/files/slides/ppsn2014-tutorial3-miller.pdf) given by Julian Miller (author of CGP):

> What defines CGP?
>
> - The genotype is a list of integers (and possibly parameters) that represent the program primitives and how the are connected together.
>   - CGP represents programs as *graphs* in which there are *non-coding genes*.
> - The genes are:
>   - Addresses in data (connection genes)
>   - Addresses in a look up table of functions
>   - Additional parameters
> - This representation is very simple, flexible, and convenient for many problems.







## How to save and load solutions

In this section, we will add two functions, `save` and `load`, in `cgp.py` to save the progress of the population.



**Save best solution**

```python
def run(self):
        # ...
        t = self.current_generation
        self.pop = cgp.evolve(self.pop, pb, MU, LAMBDA, t)
```



```python
# @todo: add t
def evolve(pop, mut_rate, mu, lambda_, t):
   
    pop = sorted(pop, key=lambda ind: ind.fitness)  # stable sorting
    parents = pop[-mu:]
    
    # @add this
    # save best solution so far
    best_sol = pop[-1]
    save(best_sol, t)
    
    # generate lambda new children via mutation
    offspring = []
    for _ in range(lambda_):
        parent = random.choice(parents)
        offspring.append(parent.mutate(mut_rate))
    return parents + offspring
```



```python
def save(best_sol, t):

    # save pop every 50 generations
    if t % 50 != 0:
        return
    
    
	

```









IMPRIME UN CHROMOSOMA PARA VER SI PODEMOS ENTENDER LAS RELACIONES ENTRE NODO ACTUAL Y NODOS ANTERIORES COMO INPUT





```python
def eval(self, *args):
    """
    Given inputs, evaluate the output of this CGP individual.
    :return the final output value
    """
    if not self._active_determined:
        self._determine_active_nodes()
        self._active_determined = True
    
    # forward pass: evaluate
    for node in self.nodes:
        if node.active:
                
            import ipdb; ipdb.set_trace()
                
            inputs = []
            for i in range(self.function_set[node.i_func].arity):
                i_input = node.i_inputs[i]
                w = node.weights[i]
                if i_input < 0:
                    # args: (79, 258, 109)
                    
                    # si i_input es un entero negativo entonces
                    # 1. lo convierte en positivo
                    # 2. lo desplaza una unidad (para que comience en 0, supongo)
                    # 3. accede al argumento correspondiente en args
                    # 4. multiplica el argumento por el peso
                    # 5. agrega el valor resultante a la lista de inputs
                    inputs.append(args[-i_input - 1] * w)
                    
                    # es decir, si i_input es un entero negativo,
                    # entonces la entrada del node proviene de args
                    # ie de los tres parametros de entrada del grafo
                    # args = ()
                    # ie, la entrada del node no proviene de otro node
                    # sino de los parametros de entrada del grafo
                else:
                    
                    # si i_input es un entero positivo entonces
                    # 1. accede al valor de salida nodo i_input
                    # 2. multiplica el valor anterior por el peso
                    # 3. agrega el valor resultante a la lista de inputs
                    inputs.append(self.nodes[i_input].output * w)
                    
            # evalua los inputs de acuerdo al operador i_func        
            node.output = self.function_set[node.i_func](*inputs)
    return self.nodes[-1].output
```













## How to improve it?

- Incorporar cruza para combinar buenas soluciones
- Cambiar la actualizacion de los pesos de un modo menos drastico:

```python
def mutate(self, mut_rate=0.01):
       
	child = copy.deepcopy(self)
    for pos, node in enumerate(child.nodes):
            
    	# mutate the function gene
        if random.random() < mut_rate:
	        node.i_func = random.choice(range(len(self.function_set)))

        # mutate the input genes (connection genes)
        arity = self.function_set[node.i_func].arity
        for i in range(arity):
                
            if node.i_inputs[i] is None or random.random() < mut_rate:  
                node.i_inputs[i] = random.randint(max(pos - self.level_back, -self.n_inputs), 
                                                  pos - 1)
            
            if node.weights[i] is None or random.random() < mut_rate:
                node.weights[i] = random.uniform(self.weight_range[0], self.weight_range[1])
```

Aqui, los weights cambian drasticamente. Seria mejor un enfoque del tipo evolucion diferencial, en donde combinemos los weights de buenas soluciones